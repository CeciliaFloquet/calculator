var express = require('express');
var router = express.Router();

/* POST add some value. */
router.post('/', function(req, res, next){
  
    var numberA= parseInt(req.body.numA) ;
    var numberB= parseInt(req.body.numB) ;

    var data = {
        result: numberA + numberB
    }
    
    res.render('add', {data});

});

/* GET render to add page */
router.get('/', (req, res, next)=> {
    var numberA= parseInt(req.query.numA) ;
    var numberB= parseInt(req.query.numB) ;

    var data = {
        result: numberA + numberB,
        valueA:numberA,
        valueB:numberB
    }
    
    res.render('add', {data});
});



module.exports = router;